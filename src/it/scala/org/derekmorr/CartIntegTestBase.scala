package org.derekmorr

import java.util.UUID

import scala.language.postfixOps

import akka.actor.{ActorRef, Props, ActorSystem}
import org.derekmorr.CartProtocol.{GetCart, Cart, AddItem}

/**
 * Base class for shopping cart integration tests.
 */
class CartIntegTestBase extends ActorTest {

  val numUsers = 20
  val minCartSize = 3
  val maxCartSize = 10
  val inventory = Seq("shoes", "junk food", "dog food", "dog treats", "dog toys", "a programming book", "frozen pizza")
  val actorSystem = this.system
  val actorPrefix = "user"

  import actorSystem.dispatcher

  /**
   * Build actors for each shopping cart
   *
   * @param userIds The sequence of user IDs
   * @param actorSystem The actorsystem in which to create the actors.
   * @param propsFactory A factory for building Props for each actor.
   * @param actorPrefix The prefix for each actor's id.
   * @return A Map of UserId -> ActorRef, containing actorref's for each cart.
   */
  def buildCartActors(userIds: Seq[UUID],
                      actorSystem: ActorSystem,
                      propsFactory: UUID => Props,
                      actorPrefix: String): Map[UUID, ActorRef] = {
    userIds map { userId =>
      (userId -> actorSystem.actorOf(propsFactory(userId), s"$actorPrefix-$userId"))
    } toMap
  }

  def populateCart(items: Seq[String], cartActor: ActorRef): Unit = {
    items foreach { item =>
      cartActor ! AddItem(item)
    }
  }

  def populateAllCarts(carts: Map[UUID, Seq[String]],
                       actorRefs: Map[UUID, ActorRef]): Unit = {

    carts.keys foreach { key =>
      val items = carts(key)
      val actorRef = actorRefs(key)
      populateCart(items, actorRef)
    }
  }

  def testCarts(userIds: Seq[UUID], generatedCarts: Map[UUID, Seq[String]], cartActors: Map[UUID, ActorRef]) = {
    userIds foreach { userId =>
      val expectedItems = generatedCarts(userId)
      val cartActor = cartActors(userId)

      val expectedMsg = Cart(expectedItems)

      cartActor ! GetCart
      expectMsg(expectedMsg)
    }
  }

}
