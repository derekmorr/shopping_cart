package org.derekmorr

import java.util.UUID

import akka.actor.{Actor, ActorLogging, Props}
import akka.event.LoggingReceive
import akka.persistence.PersistentActor

import org.derekmorr.CartProtocol.{AddItem, GetCart, Cart}
import org.derekmorr.DurableShoppingCart.ItemAdded


/**
 * A crash-tolerant shopping cart.
 *
 * Note: For simplicity, this doesn't use Akka Persistence's Snapshot feature.
 * That would significantly reduce recover time.
 */
class DurableShoppingCart(userId: UUID) extends Actor with ActorLogging with PersistentActor with SuicidalTendencies {

  override def persistenceId = userId.toString

  // forgive me
  private var items = Seq.empty[String]

  private def addItem(item: String, inRecovery: Boolean = false) = {
    log.debug(s"adding $item to cart")
    items = items :+ item

    if (!inRecovery) {
      killMyself()
    }
  }

  override def receiveRecover = {
    case ItemAdded(item) => addItem(item, true)
  }

  override def receiveCommand = {
    case AddItem(item) => persist(ItemAdded(item)) { itemAdded => addItem(itemAdded.item) }
    case GetCart       => sender() ! Cart(items)
  }

}

object DurableShoppingCart {
  def props(uuid: UUID): Props = Props(new DurableShoppingCart(uuid))

  sealed trait Events
  case class ItemAdded(item: String) extends Events
}

