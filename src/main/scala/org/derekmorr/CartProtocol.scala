package org.derekmorr

/**
 * Message protocol for shopping cart actors.
 */
object CartProtocol {
  sealed trait Commands
  case class AddItem(item: String) extends Commands
  case object GetCart extends Commands
  case class Cart(items: Seq[String]) extends Commands
}
