package org.derekmorr

import java.util.UUID

import akka.actor.{Actor, Props}
import org.derekmorr.CartProtocol.{AddItem, GetCart, Cart}

/**
 * A non-crash-tolerant shopping cart.
 */
class TransientShoppingCart(userId: UUID) extends Actor with SuicidalTendencies {

  // forgive me
  private var items = Seq.empty[String]

  private def addItem(item: String) = {
    items = items :+ item
    killMyself()
  }

  override def receive = {
    case AddItem(item) => addItem(item)
    case GetCart       => sender() ! Cart(items)
  }

}

object TransientShoppingCart {
  def props(uuid: UUID): Props = Props(new TransientShoppingCart(uuid))
}

