package org.derekmorr

import java.util.UUID

import scala.language.postfixOps
import scala.util.Random

/**
 * Mixin for generating shopping carts
 */
trait CartSupport {

  this: {
    def numUsers: Int
    def minCartSize: Int
    def maxCartSize: Int
    def inventory: Seq[String]
  } =>

  require(0 <= minCartSize)
  require(minCartSize <= maxCartSize)

  // generated test fixtures

  val userIds = genRandomUserIds(numUsers)
  val generatedCarts = buildCarts(userIds, minCartSize, maxCartSize, inventory)

  // test support methods

  def genRandomUserIds(size: Int): Seq[UUID] = Seq.fill(size)(UUID.randomUUID())

  /** Generate a random number in the range [min, max] */
  def genRandomNumber(min: Int, max: Int): Int = Math.max(Random.nextInt(min), Random.nextInt(max))

  def randomItemFromInventory(inventory: Seq[String]) = inventory(Random.nextInt(inventory.size))

  /** Generate a randomly populated cart */
  def randomCart(numberOfItems: Int, inventory: Seq[String]): Seq[String] = {
    Seq.fill(numberOfItems)(randomItemFromInventory(inventory))
  }

  /** Build random shopping carts for users */
  def buildCarts(userIds: Seq[UUID],
                 minCartSize: Int,
                 maxCartSize: Int,
                 inventory: Seq[String]): Map[UUID, Seq[String]] = {
    userIds map { userId =>
      val cartSize = genRandomNumber(minCartSize, maxCartSize)
      (userId -> randomCart(cartSize, inventory))
    } toMap
  }

}
