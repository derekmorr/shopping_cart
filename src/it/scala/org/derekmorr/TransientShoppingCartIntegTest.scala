package org.derekmorr

import java.util.UUID

import org.derekmorr.CartProtocol.{GetCart, Cart}

/**
 * Integration tests for TransientShoppingCart
 */
class TransientShoppingCartIntegTest extends CartIntegTestBase with CartSupport {

  "TransientShoppingCart" must {
    "persist changes" in {
      val propsFactory = { userId: UUID => TransientShoppingCart.props(userId) }
      val cartActors = buildCartActors(userIds, actorSystem, propsFactory, actorPrefix)

      populateAllCarts(generatedCarts, cartActors)
      testCarts(userIds, generatedCarts, cartActors)
    }
  }

}
