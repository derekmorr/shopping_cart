package org.derekmorr

import scala.util.Random

trait SuicidalTendencies {

  def biasedBoolean() = Math.abs(Random.nextGaussian()) < 0.2

  def killMyself() = {
    if (biasedBoolean()) throw new SomeRandomRuntimeException("yolo")
  }

}

class SomeRandomRuntimeException(msg: String) extends Exception

