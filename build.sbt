name := "shopping_cart"

version := "1.0"

scalaVersion := "2.11.6"

val akkaVersion = "2.3.9"

Defaults.itSettings

scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation", "-Xcheckinit",
                      "-Xlint", "-Xfatal-warnings", "-g:line",
                      "-Ywarn-dead-code",  "-Ywarn-numeric-widen")

lazy val `shopping_cart` = project.in(file(".")) configs(IntegrationTest)

// needed for akka-persistence-cassandra
resolvers += "krasserm at bintray" at "http://dl.bintray.com/krasserm/maven"

libraryDependencies ++= {
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion % Compile withSources() withJavadoc(),
    "com.typesafe.akka" %% "akka-persistence-experimental" % akkaVersion % Compile withSources() withJavadoc(),
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion % "it, runtime",
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion  % "it, test" withSources() withJavadoc(),
    "org.scalatest" %% "scalatest" % "2.2.4" % "it, test" withSources() withJavadoc(),
    "com.github.krasserm" %% "akka-persistence-cassandra" % "0.3.6" % "it, runtime",
    "ch.qos.logback" % "logback-classic" % "1.1.2" % "it, runtime"
  )
}



