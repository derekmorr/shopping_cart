# Crash-safe Shopping Cart

This is a toy implementation of a crash-safe shopping cart using the [Akka Persistence](http://doc.akka.io/docs/akka/2.3.9/scala/persistence.html). State changes are persisted into an [Apache Cassandra](http://planetcassandra.org/Try-Cassandra/) cluster, but that's configurable. It also has basic tests using the [Akka TestKit](http://doc.akka.io/docs/akka/2.3.9/scala/testing.html).

This project has two Shopping Cart implementations, `TransientShoppingCart` and `DurableShoppingCart`. The transient cart is a regular actor, so when its supervisor restarts it, its state is lost. The durable cart is a PersistentActor, so its state is reloaded on restart.

There are integration tests provided that generated random shopping carts for a random population of users. You'll see that TransientShoppingCart fails its tests, while DurableShoppingCart passes.

# Prerequisites

You will need [Java installed](http://java.oracle.com/). The app will download any additional dependencies.

You will need to edit `src/main/resources/application.conf` to set a persistence journal.

# Running tests

From the shell

    ./sbt it:testOnly

# Manually installing dependencies

The project depends on [Scala](http://www.scala-lang.org/) and its build tool, [sbt](http://www.scala-sbt.org/). If the `sbt` script fails to install Scala and SBT, you can manually install them. 

On OS X, assuming you have [homebrew](http://brew.sh/) installed, the easiest way to install these is via:

    brew install scala sbt
    
Alternatively, you can download native OS packages from the links above.
