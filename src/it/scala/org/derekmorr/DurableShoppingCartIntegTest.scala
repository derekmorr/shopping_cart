package org.derekmorr

import java.util.UUID

import org.derekmorr.CartProtocol.{GetCart, Cart}

/**
 * Integration tests for DurableShoppingCart
 */
class DurableShoppingCartIntegTest extends CartIntegTestBase with CartSupport {

  "DurableShoppingCart" must {
    "persist changes" in {
      val propsFactory = { userId: UUID => DurableShoppingCart.props(userId) }
      val cartActors = buildCartActors(userIds, actorSystem, propsFactory, actorPrefix)

      populateAllCarts(generatedCarts, cartActors)
      testCarts(userIds, generatedCarts, cartActors)
    }
  }

}
